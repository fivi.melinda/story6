from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.http import HttpResponseRedirect, HttpResponse
from django.core.validators import validate_email
from .forms import Status_Form, Subscribe_Form
from .models import Status, Subscribe
import urllib.request, json
import requests
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import logout

# Create your views here.
response = {}
response['counter'] = 0
def index(request):
    response['author'] = "FIVI" #TODO Implement yourname
    status = Status.objects.all()
    response['form'] = Status_Form
    response['model'] = status
    response['title'] = 'Update Status'
    html = 'tugas_update_status.html'
    response['status_form'] = Status_Form
    return render(request, html, response)

def add_status(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['description'] = request.POST['description']
        status = Status(description=response['description'])
        status.save()
        return HttpResponseRedirect('/tugas-update-status/')
    else:
        return HttpResponseRedirect('/tugas-update-status/')

def profile(request):
    return render(request, 'profile.html', response)

def library(request, tmp):
    if tmp == 'quilting':
        all = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting').json()
    if tmp == 'architecture':
        all = requests.get('https://www.googleapis.com/books/v1/volumes?q=architecture').json()
    if tmp == 'comic':
        all = requests.get('https://www.googleapis.com/books/v1/volumes?q=comic').json()
    items = []
    for book in all['items']:
        attr = {}
        attr['title'] = book['volumeInfo']['title']
        attr['authors'] = ", ".join(book['volumeInfo']['authors']) if 'authors' in book['volumeInfo'] else '-'
        attr['categories'] = book['volumeInfo']['categories'] if 'categories' in book['volumeInfo'] else '-'
        items.append(attr)
    return JsonResponse({"attr":items})

def subscribe_page(request):
    response = {
        'subscribe_form' : Subscribe_Form
    }
    return render(request, 'subscriber.html', response)

def subscription(request):
    if (request.method == 'POST'):
        subscribe = Subscribe(
            email = request.POST['email'],
            nama = request.POST['nama'],
            password = request.POST['password']
        )
        subscribe.save()
        return JsonResponse({
            'message':'Subscription success!'
        }, status = 200)
    else:
        return JsonResponse({
            'message':'GET method not allowed'
        }, status = 403)

def email_valid(request):
    try:
        validate_email(request.POST['email'])
    except:
        return JsonResponse({
            'message':'Sorry, that is not a valid email address',
            'status' : 'failed'
        })
    clone = Subscribe.objects.filter(email = request.POST['email'])
    if clone:
        return JsonResponse({
            'message':'Email address has been used',
            'status':'failed'
        })
    return JsonResponse({
        'message':'Email is valid',
        'status' :'success'
    })

def get_list(request):
    users = Subscribe.objects.all().values('nama', 'email')
    users_list = list(users)
    return JsonResponse(users_list, safe=False)
	
def unsubscribe(request):
    email = request.GET['email']
    Subscribe.objects.get(email=email).delete()
    return JsonResponse({})

def log_in(request):
    return render(request, 'login.html')
	
def log_out(request):
    request.session.flush()
    logout(request)
    return redirect('myBook')

@csrf_exempt
def add_fav(request):
    if request.user.is_authenticated:
        request.session['counter'] = request.session['counter'] + 1
    else:
        pass
    return HttpResponse(request.session['counter'], content_type = 'application/json')

@csrf_exempt
def unfav(request):
    if request.user.is_authenticated:
        request.session['counter'] = request.session['counter'] - 1
    return HttpResponse(request.session['counter'], content_type = 'application/json')

def myBook(request):
    if request.user.is_authenticated:
        request.session['username'] = request.user.username
        request.session['email'] = request.user.email
        if 'counter' not in request.session:
            request.session['counter'] = 0
        response['counter'] = request.session['counter']
    else:
        if 'counter' not in request.session:
            request.session['counter'] = 0
        response['counter'] = request.session['counter']
    return render(request, 'story9.html', response)