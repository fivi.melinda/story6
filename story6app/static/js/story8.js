var chosedUrl = '';
var changed = true;
var emailValid = false;

function checkEmail(){
    data = {
        'email':$('#email-input').val(),
        'csrfmiddlewaretoken': document.getElementsByName('csrfmiddlewaretoken')[0].value,
    }
    $.ajax({
        type:'POST',
        url: 'email_valid/',
        data: data,
        dataType: 'json',
        success: function(data){
            $('#notif').html('');
            if(data['status']=== 'failed'){
                emailValid = false;
                $('#subscribe-btn').prop('disabled', true);
                $('#notif').append('<small style="color:red">' + data["message"] + '</small>');
            }
            else {
                emailValid = true;
                checkAll();
                $('#notif').append('<small style="color:green">' + data["message"] + '</small>');
            }
        }
    });
}

function checkAll(){
    if (emailValid && $('#name-input').val()!= '' &&  $('#pass-input').val()!= '' ){
        $('#subscribe-btn').css('background-color', '#DCC496');
        $('#subscribe-btn').prop('disabled', false);
    } else {
        $('#subscribe-btn').css('background-color', 'darkgrey');
        $('#subscribe-btn').prop('disabled', true);
    }
}
function unsubs(email, i){
    $.ajax({
        type : 'GET',
        url : 'unsubscribe/?email='+ email,
        dataType : 'json',
        success: function(){
            $('#x-' + i).remove();
        }
    })
}
$(document).ready(function(){
    $('.apply-button').click(function(){
        if (changed){
            document.body.style.backgroundImage = "url('https://i.imgur.com/6WDy6Zr.jpg')";
            $(".accordion").css("background-color", "#003152");
            $(".content").css("background-color", "#B0DFE5");
            $("#main-title").css("background-color", "#B0DFE5");
            changed = false;
        } else {
            document.body.style.backgroundImage = "url('https://i.imgur.com/Wy2ydkN.jpg')";
            $(".accordion").css("background-color", "#836B51");
            $(".content").css("background-color", "#DCC496");
            $("#main-title").css("background-color", "#DCC496");
            changed = true;
        }
    });
    $('.accordions').find('.accordion').click(function(){
        $(this).next().slideToggle('slow');
        $(".panel").not($(this).next()).slideUp('slow');
    });

    $.fn.center = function () {
        this.css("position","absolute");
        this.css("top", Math.max(0, (
          ($(window).height() - $(this).outerHeight()) / 2) + 
           $(window).scrollTop()) + "px"
        );
        this.css("left", Math.max(0, (
          ($(window).width() - $(this).outerWidth()) / 2) + 
           $(window).scrollLeft()) + "px"
        );
        return this;
    }
      
    $("#overlay").show();
    $("#overlay-content").show().center();
      
    setTimeout(function(){    
        $("#overlay").fadeOut();
    }, 3000);

    $('.category').click(function(){
        chosedUrl = $(this).attr('id');
        $.ajax({
            type : 'GET',
            url : chosedUrl,
            dataType : 'json',
            success : function(data) {
                var val = '<tr>';
                for ( var i = 1; i<=data.attr.length; i++){
                    val += '<th scope ="row">' + i + '</th> <td>' + data.attr[i-1].title + '</td> <td>';
                    val += data.attr[i-1].authors;
                    val += '</td> <td>' + data.attr[i-1].categories + '</td> <td>';
                    val += '<button id="button" type="button" class="fav-btn"><i id="favorite" class="fa fa-heart"></i></button></td></tr>';
                }
                $('#myTable tbody').empty();
                $('#myTable tbody').append(val);
            }

        });
    });

    $(document).on('click', '#favorite', function() {
        if($(this).hasClass('clicked')){
            $(this).removeClass('clicked');
            $(this).css("color", "white");
            $.ajax({
                url: 'unfav',
                dataType: 'json',
                success: function(result){
                    $('.count').html(result);
                }
            })
            
        } else {
            $(this).addClass('clicked');
            $(this).css("color", "red");
            $.ajax({
                url: 'add_fav',
                dataType: 'json',
                success: function(result){
                    $('.count').html(result); 
                }
            })
            
        }
        
    });

    $('input').focusout(function(){
        checkAll();
    });
    $('#email-input').keyup(function(){
        checkEmail();
    });
    $('input').focusout(function() {
        checkEmail();
    });
    $('#pass-input').keyup(function(){
        checkAll();
    });

    $('#subscribe-btn').click(function(){
        data = {
            'email': $('#email-input').val(),
            'nama' : $('#name-input').val(),
            'password': $('#pass-input').val(),
            'csrfmiddlewaretoken': document.getElementsByName('csrfmiddlewaretoken')[0].value,
        }
        $.ajax({
            type:'POST',
            url: 'subscription',
            data: data,
            dataType: 'json',
            success: function(data){
                alert(data['message']);
                $('#subscribe-btn').css('background-color', 'darkgrey');
                $('#subscribe-btn').prop('disabled', true);
                $('#email-input').val('');
                $('#name-input').val('');
                $('#pass-input').val('');
                $('#notif').html('');
            }
        });
    });

        $.ajax({
            type : 'GET',
            url : 'get_list/',
            dataType : 'json',
            success : function(person){
                var val = '';
                for ( var i = 0; i<person.length; i++){
                    val += "<tr  id='x-" + i + "'><td>" + person[i].nama + "</td><td>"
                    val += person[i].email +"</td>"
                    val += "<td><button type='button' class='del-list' onClick=unsubs('"+ person[i].email + "'," + i+")>Delete</button></td></tr>"
                }
                $('#mysubscriber').append(val);
            }

        });

});


