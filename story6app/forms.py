from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    description_attrs = {
        'type': 'text',
        'rows': 4,
        'class': 'status-form-textarea',
        'placeholder':'What\'s on your mind?'
    }

    description = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))

class Subscribe_Form(forms.Form):
    email = forms.EmailField(
         required = True, 
         max_length=30, 
         widget=forms.TextInput(attrs={
             'type':'text',
             'placeholder': 'youremail@gmail.com',
             'id' : 'email-input'
        })
    )
    nama = forms.CharField(
         required=True, 
         max_length=30, 
         widget=forms.TextInput(attrs={
             'type':'text',
             'placeholder':'your name',
             'id': 'name-input'
        })
    )
    password = forms.CharField(
        required=True, 
        max_length=20,
        widget=forms.TextInput(attrs={
            'type': 'password', 
            'placeholder': 'password',
            'id' : 'pass-input'
        })
    )

