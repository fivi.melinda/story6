from django.db import models

# Create your models here.
class Status(models.Model):
    description = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)

class Subscribe(models.Model):
    email = models.CharField(max_length=30, unique=True)
    nama = models.CharField(max_length=30)
    password = models.CharField(max_length=20)