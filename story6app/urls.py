from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views
from .import views

urlpatterns = [
    url(r'^$', views.profile, name='profile'),
    url(r'^add_status', views.add_status, name='add_status'),
    url(r'^tugas-update-status', views.index, name='index'),
    url(r'^lib/(?P<tmp>[\w\-]+)/$', views.library, name='library'),
    url(r'^books', views.myBook, name="myBook"),
    url(r'^subscribe', views.subscribe_page, name='subscribe_page'),
    url(r'^subscription', views.subscription, name='subscription'),
    url(r'^email_valid', views.email_valid, name='email_valid'),
    url(r'^get_list', views.get_list, name='get_list'),
    url(r'^unsubscribe', views.unsubscribe, name='unsubscribe'),
    url(r'^login/$', views.log_in, name='login'),
    url(r'^logout/$', views.log_out, name='logout'),
    url(r'^auth/', include('social_django.urls', namespace='social')),  # <- Here
    url(r'^unfav', views.unfav, name='unfav'),
    url(r'^add_fav', views.add_fav, name='add_fav'),
]

