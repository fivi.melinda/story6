from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, add_status, profile, myBook, library, subscribe_page, email_valid, subscription
from .models import Status, Subscribe
from .forms import Status_Form, Subscribe_Form

# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
# from selenium.webdriver.support.color import Color

# Create your tests here.
class UnitTest(TestCase):
    def test_updatestatus_6_url_is_exist(self):
        response = Client().get('/tugas-update-status/')
        self.assertEqual(response.status_code, 200)

    def test_story_6_using_hello_world(self):
        request = HttpRequest()
        response = index(request)
        string = response.content.decode("utf8")
        self.assertIn("Hello, Apa kabar?", string)
    
    def test_updatestatus_navbar_profile_is_exist(self):
         request = HttpRequest()
         response = index(request)
         string = response.content.decode("utf8")
         self.assertIn("fa fa-user-circle", string)

    def test_story_6_using_index_func(self):
        found = resolve('/tugas-update-status/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_status(self):
        # Creating a new activity
        new_activity = Status.objects.create(description='mengerjakan tugas ppw')

        # Retrieving all available activity
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_form_status_input_has_placeholder_and_css_classes(self):
        form = Status_Form()
        self.assertIn('class="status-form-textarea', form.as_p())
        self.assertIn('id="id_description', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'description': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['description'],
            ["This field is required."]
        )
    def test_story6_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/add_status', {'description': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/tugas-update-status/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_story6_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/add_status', {'description': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/tugas-update-status/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_profile_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_story6_title_is_exist(self):
         request = HttpRequest()
         response = profile(request)
         string = response.content.decode("utf8")
         self.assertIn("ABOUT WRITER", string)

    def test_story6_activity_is_exist(self):
         request = HttpRequest()
         response = profile(request)
         string = response.content.decode("utf8")
         self.assertIn("ACTIVITY", string)

    def test_story9_url_is_exist(self):
        response = Client().get('/books')
        self.assertEqual(response.status_code, 200)
    def test_using_story9_func(self):
        found = resolve('/books')
        self.assertEqual(found.func, myBook)
    def test_story9_return_jsonresponse(self):
        response = Client().get('/lib/architecture/')
        self.assertEqual(response.status_code,200)
    def test_story9_return_jsonresponse2(self):
        response = Client().get('/lib/comic/')
        self.assertEqual(response.status_code,200)
    def test_story9_return_jsonresponse3(self):
        response = Client().get('/lib/quilting/')
        self.assertEqual(response.status_code,200)

    def test_story10_url_is_exist(self):
        response = Client().get('/subscribe')
        self.assertEqual(response.status_code, 200)
    def test_story10_create_new_model(self):
        Subscribe.objects.create(email='newemail@gmail.com',nama='newuser', password='newuser')
        counter = Subscribe.objects.all().count()
        self.assertEqual(counter, 1)
    def test_story10_email_invalid(self):
        response = Client().post('/email_valid', data={'email':'emailemail'})
        self.assertEqual(response.json()['status'], 'failed')
    def test_story10_email_hasbeenUsed(self):
        Subscribe.objects.create(email="useremail@gmail.com", nama="user", password="password")
        response = Client().post('/email_valid', data={"email":"useremail@gmail.com"})
        self.assertEqual(response.json()['status'], 'failed')
    def test_story10_email_valid(self):
        response = Client().post('/email_valid', data={"email":"newemail@gmail.com"})
        self.assertEqual(response.json()['status'], 'success')



# class FunctionalTest(TestCase):
#     def setUp(self):
#         super(FunctionalTest,self).setUp()
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         service_log_path = "./chromedriver.log"
#         service_args = ['--verbose']
#         self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         self.selenium.implicitly_wait(5) 
    
#     def tearDown(self):
#         self.selenium.quit()
#         super(FunctionalTest, self).tearDown()
    
#     def test_input_todo(self):
#         selenium = self.selenium
#         #opening the link we want to test
#         selenium.get('http://story6-fivi.herokuapp.com/tugas-update-status/')
#         #find the form element
#         description = selenium.find_element_by_class_name('status-form-textarea')

#         submit = selenium.find_element_by_id('post-button')

#         #fill the form
#         description.send_keys('coba-coba')

#         #submit form
#         submit.send_keys(Keys.RETURN)
#         #check status is exist
#         self.assertIn('coba-coba', selenium.page_source)
    
#     def test_website(self):
#         selenium = self.selenium
#         selenium.get('http://story6-fivi.herokuapp.com/tugas-update-status/')
#         title = selenium.find_element_by_tag_name('form').text
#         self.assertIn('Hello, Apa kabar?', title)
#         button = selenium.find_element_by_id('post-button').get_attribute("value")
#         self.assertIn('Post', button)
#         rgb = selenium.find_element_by_css_selector('.icon-bar').value_of_css_property('background-color')
#         hex = Color.from_string(rgb).hex
#         self.assertEqual('#555555', hex)
#         align = selenium.find_element_by_css_selector('.icon-bar a').value_of_css_property('text-align')
#         self.assertEqual('center', align)